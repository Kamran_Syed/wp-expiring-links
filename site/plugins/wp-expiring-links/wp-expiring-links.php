<?php
/*
Plugin Name: WP Expiring Links
Plugin URI: http://codecanyon.net/user/QualitynAgility
Description: It creates links/URLs that expire after certain number of click or certain number of days after first click whichever comes first. 
Version: 1.1
Author: QualitynAgility
Author URI: http://codecanyon.net/user/QualitynAgility
*/

if ( !class_exists( 'agile_link_generator' )){   
	class agile_link_generator{
	
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));			
			add_action( 'wp_ajax_aspk_expiry_link',  array(&$this,'aspk_expiry_link' ));
			add_action( 'wp_ajax_nopriv_aspk_expiry_link', array(&$this,'aspk_expiry_link' ));
			add_action( 'update_remaining_days_hook', array(&$this,'update_remaining_days' ));
		}
		
		function aspk_expiry_link(){
		
			if(isset($_GET['aspk_hash'])){
				if(isset($_GET['link_id'])){
					$row_id = $_GET['link_id'];
					$hash = $_GET['aspk_hash'];
					
					$link_details = $this->get_link_request($row_id,$hash);

					if(!$link_details){
						include( get_query_template( '404' ) );
						exit;
					}

					
					$first_click_dt = $this->get_dt_from_table($row_id,$hash);
					
					if($first_click_dt->first_click_dt){
					
						$current_dt = $this->get_current_dt_time();
						$diff_days = $this->get_dt_diff($first_click_dt->first_click_dt,$current_dt);
						
					}else{
					
						$this->add_first_click_dt($row_id);
						
					}
					
					if($this->validate_allow_click_with_total_clicks($link_details,$diff_days) === true) {
							
							$device_details = $this->prepare_info();
							$this->insert_device_n_browser_details($device_details,$row_id);
							$has_dt = $this->update_total_clicks($link_details);
							$this->update_remainig_clicks($link_details);
							$source = $link_details->source;
							
							if($this->if_url_is_file($source) === true){
								$this->stream_file($source);
							}else{
								wp_redirect($source, 302);
								exit;
							}
					}else{
							$location = $this->get_expiray_page();
							if($location){
								wp_redirect( $location, 302);
								exit;
							}
					}
				}
			}
			exit;
		}
		
		
		function update_remaining_days(){
		
			$results = $this->get_first_click_dates();
			$current_dt = $this->get_current_dt_time();
			if($results){
				foreach($results as $rs){
					if($rs->first_click_dt){
						$days_diff = $this->get_dt_diff($rs->first_click_dt,$current_dt);
						$rem_days = $rs->allowed_days - $days_diff;
						$this->update_remaining_days_for_link($rs->id,$rem_days);
					}
				}
			}
		}
		
		function update_remaining_days_for_link($id,$days){
				global $wpdb;
				
				if($days < 0) $days = 0;
				$sql = "update {$wpdb->prefix}aspk_link_generator set  remaining_days = {$days} where id = {$id}";
				$wpdb->query($sql);	

		}
		
		function get_first_click_dates(){
		
			global $wpdb;
			
			$sql = "select * from {$wpdb->prefix}aspk_link_generator";
			$results = $wpdb->get_results($sql);
			
			return $results;
		
		}
		
		function update_remainig_clicks($link_details){
			global $wpdb;
			
			$id = $link_details->id;
			$rem_clicks = $link_details->remaining_clicks - 1;
			if($id){
				$sql = "update {$wpdb->prefix}aspk_link_generator set  remaining_clicks = {$rem_clicks} where id = {$id}";
				$wpdb->query($sql);
			}
		
		}
		
		function validate_link($row_id,$hash){
			global $wpdb;
			
			$sql = "select * from {$wpdb->prefix}aspk_link_generator where id = {$row_id} AND hash = {$hash}";
			$results = $wpdb->get_row($sql);
			$site_url = site_url();
			$string_found = strpos($results->source,$site_url);
			if($string_found !== false){
				return true;
			}
			return false;
		}
		
		function stream_file($url){
			$x = str_replace(home_url().'/','',$url);
			$relative_path = wp_make_link_relative($x);
			$full_path = ABSPATH.$relative_path;
			if(file_exists($full_path)) {
				ob_end_clean();
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".basename($full_path));
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: no-cache');
				header('Pragma: no-cache');
				ob_clean();
				flush();
				readfile($full_path);
				exit;	
			}
		}
		
		function if_url_is_file($url){
			$x = str_replace(home_url().'/','',$url);
			$relative_path = wp_make_link_relative($x);
			$full_path = ABSPATH.$relative_path;
			if(file_exists($full_path)){
				return true;
			}else{		
				return false;
			}
		}
		
		
		function insert_device_n_browser_details($device_details,$row_id){
		
			global $wpdb;
			
			$access_data = serialize($device_details); 
			$current_dt = $this->get_current_dt_time();
			$sql = "INSERT INTO {$wpdb->prefix}aspk_link_access_detail(link_id,access_data,current_dt) VALUES({$row_id},'{$access_data}','{$current_dt}')";
			$wpdb->query($sql);
		}
		
		function get_dt_diff($first_click_dt,$current_dt){
			$date1 = new DateTime($first_click_dt);
			$date2 = new DateTime($current_dt);
			return $date2->diff($date1)->format("%a");
		
		}
		
		function add_first_click_dt($id){
			global $wpdb;
			
			if($id){
				$dt = $this->get_current_dt_time();
				$sql = "update {$wpdb->prefix}aspk_link_generator set  first_click_dt = '{$dt}' where id = {$id}";
				$wpdb->query($sql);
			}
		}
		
		function get_dt_from_table($row_id,$hash){
		
			global $wpdb;
			
			$sql = "select first_click_dt from {$wpdb->prefix}aspk_link_generator where id = {$row_id} AND hash = {$hash}";
			$results = $wpdb->get_row($sql);
			
			return $results;
		
		}
		
		function get_expiray_page(){
			$get_link_Settings = $this->get_settings();
			$page_id = $get_link_Settings['page_dropdown'];
			$slug = get_page( $page_id );
			$expiry_page_link = $slug->guid;
			return $expiry_page_link;
		}
		
		function update_total_clicks($link_details){
			
			global $wpdb;
			
			$id = $link_details->id;
			$sql = "update {$wpdb->prefix}aspk_link_generator set  total_clicks = total_clicks + 1 where id = {$id}";
			$wpdb->query($sql);
		
		}
		
		function validate_allow_click_with_total_clicks($link_details,$rem_days){
			if(($link_details->total_clicks < $link_details->allowed_clicks) && ($rem_days < $link_details->allowed_days)){
				return true;
			}
			return false;
		}
		
		function get_link_request($row_id,$hash){
		
			global $wpdb;
			//check for blank para
			$sql = "select * from {$wpdb->prefix}aspk_link_generator where id = {$row_id} AND hash = {$hash}";
			$results = $wpdb->get_row($sql);
			
			return $results;
		}
		
		function get_browser_info(){
			if (function_exists('dirname')) {
				require_once dirname(__FILE__) .'/ua-parser-php/UAParser.php';
			} else {
				require_once __DIR__ .'/ua-parser-php/UAParser.php';
			}
			
			
			
			$ua = $_SERVER['HTTP_USER_AGENT'];
			$result = UA::parse($ua);
			return $result;
		}
		
		function get_settings(){
		
			$defaults = array();
			$defaults['gene_name'] 	= '';
			$defaults['current_date']	= '';
			$defaults['source_ip'] 	= '';
			$defaults['proxy_ip']		= '';
			$defaults['device_used'] 	= '';
			$defaults['browser_used'] 	= '';
			$defaults['operating_system']= '';
			$defaults['web_url_request'] = '';
			$defaults['full_path'] 	  = '';
			$defaults['page_dropdown'] 	  = '';
			$defaults['info_password']   = '';
			$defaults['display_text_link'] = '';
			$defaults['display_button_link'] = '';
			
			$opt = get_option('aspk_link_Settings',$defaults);
			return $opt;
		}
		
		function prepare_info(){
			$info = array();
			$settings = $this->get_settings();
			$info['username'] = "";
			$info['current_date'] = "";
			$info['source_ip'] = "";
			$info['proxy_ip'] = "";
			$info['device_used'] = "";
			$info['browser_used'] = "";
			$info['operating_system'] = "";
			$info['web_url_request'] = "";
			$info['full_path'] = "";
			
				$current_user = wp_get_current_user();
				if ( !($current_user instanceof WP_User) ){
					$info['username'] = 'guest';
				}elseif(0 == $current_user->ID){
					$info['username'] = 'guest';
				}else{
					$info['username'] = $current_user->user_login;
				}
			
				$info['current_date'] =  date('Y-m-d h:i:s');
				
			
				$info['source_ip'] =  $_SERVER['REMOTE_ADDR'];
				if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
					$info['source_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
					$info['proxy_ip'] = $_SERVER['REMOTE_ADDR'];
				}
			
			$browser = $this->get_browser_info();
				$device = "Other";
				if($browser->isMobileDevice == '1' || $browser->isMobile == 1){
					$device = "Mobile Device";
				}elseif($browser->isTablet == '1'){
					$device = "Mobile Device";
				}elseif($browser->isComputer == '1'){
					$device = "Computer";
				}
				
				$info['device_used'] =  $device;
			
				$info['browser_used'] =  $browser->browserFull;
			
				$info['operating_system'] =  $browser->osFull;
			
					$info['web_url_request'] =  $this->get_current_url();
				
			
			
			$info['has_info'] = '4298';
			return $info;
		}
		
		function get_current_url($full = true) {
			if (isset($_SERVER['REQUEST_URI'])) {
				$parse = parse_url(
					(isset($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'off') ? 'https://' : 'http://') .
					(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '')) . (($full) ? $_SERVER['REQUEST_URI'] : null)
				);
				$parse['port'] = $_SERVER["SERVER_PORT"]; // Setup protocol for sure (80 is default)
				
				return $parse['scheme'].'://'.$parse['scheme'].$parse['host'].$parse['path'];  //':'.$parse['port'];
			}
		}
		
		function install(){
		global $wpdb;	
			
			$sql="
			CREATE TABLE `".$wpdb->prefix."aspk_link_generator` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `source` varchar(255) DEFAULT NULL,
			  `target` varchar(500) DEFAULT NULL,
			  `hash` varchar(255) DEFAULT NULL,
			  `allowed_clicks` int(30) DEFAULT NULL,
			  `total_clicks` int(30) DEFAULT NULL,
			  `remaining_clicks` int(30) DEFAULT NULL,
			  `allowed_days` varchar(255) DEFAULT NULL,
			  `remaining_days` varchar(255) DEFAULT NULL,
			  `created_dt` datetime DEFAULT NULL,
			  `first_click_dt` datetime DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
		
			$sql1="
			CREATE TABLE `".$wpdb->prefix."aspk_link_access_detail` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `link_id` int(11) NOT NULL,
				  `access_data` text NOT NULL,
				  `current_dt` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql1);
			$x = wp_schedule_event( time(), 'hourly', 'update_remaining_days_hook' );
		}
		
		function wp_enqueue_scripts(){

			wp_enqueue_script('jquery');
			wp_enqueue_style( 'tw-bs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			wp_enqueue_script( 'collapsible', plugins_url('js/jquery.collapsible.js', __FILE__) );
		}
		
		function admin_menu(){
			add_menu_page('Expiring Link Generator', 'Expiring Link Generator', 'manage_options', 'aspk_link_generator', array(&$this, 'link_generator'));
			add_submenu_page('aspk_link_generator','Links Report', 'Links Report', 'manage_options', 'aspk_display_details', array(&$this,'Details_links'));
			add_submenu_page('','Display link', 'Display link', 'manage_options', 'aspk_display_link', array(&$this,'display_link'));
			add_submenu_page('aspk_link_generator','Link Settings', 'Links Settings', 'manage_options', 'aspk_link_Settings', array(&$this,'link_Settings'));
		}
		
		function get_all_link_details(){
		
			global $wpdb;
			
			$sql = "select * from {$wpdb->prefix}aspk_link_generator";
			$results = $wpdb->get_results($sql,ARRAY_A);
			
			return $results;
		}
		
		function Details_links(){
			if(isset($_GET['act'])){
				if($_GET['act'] == 'del'){

					$this->delete_link_details($_GET['link_id']);
					$this->delete_link_device_details($_GET['link_id']);
				}
			}
			$link_details = $this->get_all_link_details();	
			if($link_details){
			?>
			<div class="tw-bs container" style = "padding:1em;margin-top:1em;background-color:white; border: 1px solid;float:left;"><!-- start container -->
					<div  class="row" style="clear:left;">
					<div style="margin-top: 1em; " class="col-md-2"><h4>Date Created</h4></div>
					<div style="margin-top: 1em;" class="col-md-4"><h4>Target Link</h4> </div>
					<div style="margin-top: 1em;" class="col-md-2"><h4>Remaining Clicks</h4></div>
					<div style="margin-top: 1em;" class="col-md-2"><h4>Remaining Days</h4></div>
					<div style="margin-top: 1em;" class="col-md-2"></div>
					</div>
				<?php foreach($link_details as $link_detail){?>
				<div  class="row" style="clear:left;">
					<div  style="margin-top:1em; "class="col-md-2"><?php echo $link_detail['created_dt']; ?></div>
					<div  style="margin-top:1em; "class="col-md-4"><?php echo $link_detail['target']; ?></div>
					<div style="margin-top: 1em;  "class="col-md-2"><?php echo $link_detail['remaining_clicks']; ?></div>
					<div style="margin-top: 1em;  "class="col-md-2"><?php echo $link_detail['remaining_days']; ?></div>
					<div style="margin-top:1em;" class="col-md-1"><a style="width:3.8em;" href="<?php echo admin_url('admin.php?page=aspk_display_details&act=del&link_id='.$link_detail['id']);?>" class="btn btn-danger">X</a></div>
					<div class = "col-md-1" style = "margin-top:1em;">
					<form method="post" action="<?php echo admin_url('admin.php?page=aspk_display_link');?>">
							<input type="hidden" name="view_link_id" value="<?php echo $link_detail['id'];?>"/>
							<input class="btn btn-primary" type="submit" name="view_details" value="View"/>
					</form>
					
					</div>					
				</div>
					<?php } ?>
			</div>
	
		<?php
			}else{	?>
				<div class="tw-bs container error" style = "padding:1em;margin-top:1em;background-color:white; border: 1px solid;float:left; width:90%;"><!-- start container -->
					<div class="error">No Result Found</div>
				</div>
			<?php
			}
		}
		
		function get_insert_id(){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}aspk_link_generator(source,target,hash,allowed_clicks,total_clicks,allowed_days) VALUES('','','','','','')";
			$wpdb->query($sql);
			$inserted_id = $wpdb->insert_id;
			return $inserted_id;
		}
		
		function get_hash(){
			$hash = rand(10000, 19999);
			return $hash;
		}
		
		function create_target_link($inserted_id,$hash){
			$target_url =  admin_url('admin-ajax.php?action=aspk_expiry_link&link_id='.$inserted_id.'&aspk_hash='.$hash);
			return $target_url;
		}
		
		function update_link_details($dt,$inserted_id,$source,$target_url,$hash,$allowed_clicks,$aspk_allow_days){
			global $wpdb;
			
			$sql = "update {$wpdb->prefix}aspk_link_generator set source = '{$source}' , target = '{$target_url}',hash = '{$hash}' , allowed_clicks = {$allowed_clicks},remaining_clicks = {$allowed_clicks},allowed_days = {$aspk_allow_days},remaining_days = {$aspk_allow_days},created_dt = '{$dt}' where id = {$inserted_id}";
			$wpdb->query($sql);
			
		}
		
		function get_current_dt_time(){
			$date = new DateTime();
			$dt = $date->format('Y-m-d H:i:s');
			return $dt;
		
		}
		
		function get_new_link_details($id){
			global $wpdb;
			
			$sql = "select * from {$wpdb->prefix}aspk_link_generator where id = {$id}";
			$results = $wpdb->get_row($sql,ARRAY_A);
			
			return $results;
		
		}
		
		function delete_link_details($id){
			global $wpdb;
			
			$sql = "Delete from {$wpdb->prefix}aspk_link_generator where id = {$id} limit 1";
			$wpdb->query($sql);
		
		}
		
		function delete_link_device_details($id){
			global $wpdb;
			
			if($id){
				$sql = "Delete from {$wpdb->prefix}aspk_link_access_detail where link_id = {$id}";
				$wpdb->query($sql);
			}
		}
		
		function select_link_to_edit($id){
			global $wpdb;
			
			$sql = "select * from {$wpdb->prefix}aspk_link_generator where id = {$id}";
			$results = $wpdb->get_row($sql);
			return $results;
		
		}
		
		
		function validate_if_url_has_dir($url){
			$string_found = strstr($url,site_url());
			if($string_found){
				$x = str_replace(home_url().'/','',$url);
				$relative_path = wp_make_link_relative($x);
				$full_path = ABSPATH.$relative_path;
				if(is_dir($full_path) === true){
					?>
					<div style="width:90%;margin-top:1em;border:1px solid;border:color:black;padding:1em;background-color:white;">
							<div style='padding:1em;' class='error'><strong>URL</strong> is not valid</div>
						<div style="clear:both;margin-left:1em;"><input type="button" value="Back" class="button-primary" onclick="window.history.back();"/> </div>
					</div>
					<?php
					return false;
				}
			}
			return true;
		}
		
		function link_generator(){
				
			
			if(isset($_POST['aspk_link_Settings'])){
				if(isset($_GET['act'])){
					if($_GET['act'] == 'edit'){
						$id = $_GET['link_id'];
						if($id){
							$source = $_POST['Enter_Link'];
							$aspk_allow_days = $_POST['allow_dy'];
							$allowed_clicks = $_POST['allow_clicks'];
							$ret = $this->validate_if_url_has_dir($source);
							if($ret === false) return;
							$hash = $this->get_hash();
							$target_url  = $this->create_target_link($id,$hash);
							$dt = $this->get_current_dt_time();
							$this->update_link_details($dt,$id,$source,$target_url,$hash,$allowed_clicks,$aspk_allow_days);
							$new_link_details = $this->get_new_link_details($id);
							
						}
					}
				}else{
					$ret = $this->validate_if_url_has_dir($_POST['Enter_Link']);
					if($ret === false) return;
					$source = $_POST['Enter_Link'];
					$aspk_allow_days = $_POST['allow_dy'];
					$allowed_clicks = $_POST['allow_clicks'];
					$inserted_id= $this->get_insert_id();
					$hash = $this->get_hash();
					$target_url  = $this->create_target_link($inserted_id,$hash);
					$dt = $this->get_current_dt_time();
					$this->update_link_details($dt,$inserted_id,$source,$target_url,$hash,$allowed_clicks,$aspk_allow_days);
					$new_link_details = $this->get_new_link_details($inserted_id);
				}
			}
			if(isset($_GET['act'])){
				if($_GET['act'] == 'del'){

					$this->delete_link_details($_GET['link_id']);
				}
			}
			
			if(isset($_GET['act'])){
				if($_GET['act'] == 'edit'){
					$edit_details = $this->select_link_to_edit($_GET['link_id']);
				}
			}
			
			if(isset($_POST['aspk_link_Settings'])){
				
			?>
			<div class="tw-bs container" style = "width:90%;float:left;padding:1em;margin-top:1em;background-color:white; border: 1px solid;"><!-- start container -->
					<div  class="row" style="clear:left;">
						<div style="margin-top: 1em; margin-left:1em;" class="col-md-12"><h4>Source Link</h4></div>
					</div>
					<div  class="row" style="clear:left;">
						<div style="margin-top: 1em; margin-left:1em;" class="col-md-12"><?php if(!empty($new_link_details['source'])){echo $new_link_details['source']; }?> </div>
					</div>
					
					<div  class="row" style="clear:left;">
					<div style="margin-top: 1em; margin-left:1em;" class="col-md-12"><h4>Target Link</h4> </div>
					</div>
					
					<div  class="row" style="clear:left;">
					<div style="margin-top: 1em; margin-left:1em;" class="col-md-12"><?php echo $new_link_details['target']; ?></div>
					</div>
				<div  class="row" style="clear:left;">
					<div  style="margin-top:1em;" class="col-md-2"><a style="width:6em;" href="<?php echo admin_url('admin.php?page=aspk_link_generator&act=del&link_id='.$new_link_details['id']);?>" class="btn btn-danger">Delete</a></div>
					<div  style="margin-top:1em; "class="col-md-2"><a style="width:6em;" href="<?php echo admin_url('admin.php?page=aspk_link_generator&act=edit&link_id='.$new_link_details['id']);?>" class="btn btn-primary">Edit</a></div>
					<div  style="margin-top:1em;" class="col-md-2"><a style="width:6em;" href="<?php echo admin_url('admin.php?page=aspk_link_generator');?>" class="btn btn-primary">Add link</a></div>
					<div  style="margin-top:1em;" class="col-md-6"></div>
				</div>
											
			</div>
			<?php } else{?>
			<div class="tw-bs container" style = "width:90%;padding:1em;margin-top:1em;background-color:white; border: 1px solid;"><!-- start container -->
				<form  id="aspk_link_generate" method="post" action="" >
				   <div style="clear:left;" class="row">
					 <div class="col-md-12" style="margin-left:1em;"><h3>Expiry Link Generator</h3>
					 </div>
				  </div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-2" style="margin-top: 1em; margin-left:1em;font-size: initial;">Enter Link</div>
						<div  class="col-md-9" style="margin-top:1em;">
							<input id="focus_fld" required="required" class="form-control" type = "text"  value="<?php if(isset($edit_details->source)){echo $edit_details->source;} ?>" name = "Enter_Link" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" >	
						</div>	
					</div>	
					<div id="sam_hide"style="margin:auto;text-align:center;display: none;clear:left;"><small style="color:red;">Please Give Valid Address </small></div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-2" style="margin-top: 1em; margin-left:1em;font-size: initial;">Allowed Clicks</div>
						<div  class="col-md-9" style="margin-top:1em;">
							<input required="required"  id="isnum" class="form-control" type = "Number"  value="<?php if(isset($edit_details->allowed_clicks)){echo $edit_details->allowed_clicks;}else{echo 3 ;} ?>" name = "allow_clicks" />
						</div>
						
						<div  class="col-md-1" style="margin-top:1em;" >	
						</div>	
					</div>
					<div id="num_hide"style="margin:auto;text-align:center;display: none;clear:left;"><small style="color:red;">Please Give Positive Number </small></div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-2" style="margin-top: 1em; margin-left:1em;font-size: initial;">Allowed Days</div>
						<div  class="col-md-9" style="margin-top:1em;">
							<input id="dy_vl" required="required"  class="form-control" type = "Number"  value="<?php  if(isset($edit_details->allowed_clicks)){ echo $edit_details->allowed_days;}else{ echo 3 ;} ?>" name = "allow_dy" />
						</div>
						
						<div  class="col-md-1" style="margin-top:1em;" >	
						</div>	
					</div>
					<div id="num_days"style="margin:auto;text-align:center;display: none;clear:left;"><small style="color:red;">Please Give Positive Number </small></div>

					<div  class="row" style="clear:left;">
						<div style="margin-top: 1em; margin-left:1em;" class="col-md-12">
								<input class = "btn btn-primary" type="submit" value="Generate" name="aspk_link_Settings">
						</div>
					</div>
					<div  class="row" style="clear:left;">
						<div class="alert alert-warning" style="text-align:center;width:97%;margin-top: 5em; margin-left:1em;" class="col-md-12">
								<strong>Link will expire when either of above exceeds.</strong>
						</div>
					</div>
					<div  class="row" style="clear:left;">
							<div style="margin-top: 1em; margin-left:1em;" class="col-md-12"></div>
					</div>
				</form>
			</div>
			<?php
			}
			?>		
			<script>
				jQuery("#aspk_link_generate").submit(function(event){
						var s = jQuery('#focus_fld').val();
						var x = s.indexOf("http:" || "https:");
						if(x == -1){
							jQuery("#sam_hide").show();
							event.preventDefault();
						}
						if(jQuery("#isnum").val() < 1){
							jQuery("#num_hide").show();
							event.preventDefault();
						}
						if(jQuery("#dy_vl").val() < 1){
							jQuery("#num_days").show();
							event.preventDefault();
						}
					
				});
				jQuery("#focus_fld").focus(function(){
					jQuery("#sam_hide").hide();
				});
				jQuery("#isnum").focus(function(){
					jQuery("#num_hide").hide();
				});
				jQuery("#dy_vl").focus(function(){
					jQuery("#num_days").hide();
				});
				
			
			</script>
			<?php
		}
			
		function get_detail_from_link_generator_details($id){ 
			global $wpdb;
			
			$sql="SELECT *  FROM `{$wpdb->prefix}aspk_link_generator`, `{$wpdb->prefix}aspk_link_access_detail` WHERE {$wpdb->prefix}aspk_link_generator.id = {$id} AND {$wpdb->prefix}aspk_link_access_detail.link_id ={$id}";
			$results = $wpdb->get_results($sql);
			return $results;
		
		}	
			
		function display_link(){
			if(isset($_POST['view_details'])){
				$id = $_POST['view_link_id'];
				if($id){
					$results = $this->get_detail_from_link_generator_details($id);
				}
			}
			$settings  = $this->get_settings();
			?>
			<div class="tw-bs container" style = "width:90%;float:left;padding:1em;margin-top:1em;background-color:white; border: 1px solid; overflow: hidden;"><!-- start container -->


				<?php 
				if(!empty($results)){ ?>
					<div class="row" style="float:left;clear:left;">
							<div style="margin-left:1em;" class="col-md-12"><h3>Link Details</h3></div>
					</div>
				<?php
				foreach($results  as $result){ 
							$device_data = unserialize($result->access_data);
					?>
						<div style="padding:1em;margin-top:0.2em;width:100%;clear:left;"class="well collapsible" id="nav-section1">Link<span></span></div>
						<div>
								<?php if($settings['gene_name'] == 'on' && $device_data['username']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>User Name</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $device_data['username'];?></div>
									</div>
								<?php } ?>
								<?php if($result->target){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Target URL</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $result->target;?></div>
									</div>
								<?php } ?>
								<?php if($result->source){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Source URL</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $result->source;?></div>
									</div>
								<?php } ?>
								<?php if($result->allowed_clicks){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Allowed Clicks</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $result->allowed_clicks; ?></div>
									</div>
								<?php } ?>
								<?php if($result->remaining_clicks){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Remaining Clicks</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $result->remaining_clicks; ?></div>
									</div>
								<?php } ?>
								<?php if($settings['current_date'] == 'on'){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Current Date Time</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo date('Y-m-d h:i:s'); ?></div>
									</div>
								<?php } ?>
								<?php if($result->first_click_dt){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>First Click On</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo $result->first_click_dt; ?></div>
									</div>
								<?php } ?>
								<?php if($settings['browser_used'] == 'on' && $device_data['browser_used']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Browser Used</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo  $device_data['browser_used']; ?></div>
									</div>
									<?php } ?>
								<?php if($settings['operating_system'] == 'on' && $device_data['operating_system']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Operating System Used</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo  $device_data['operating_system']; ?></div>
									</div>
								<?php } ?>
								<?php if($settings['device_used'] == 'on' && $device_data['device_used']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Device Used</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo  $device_data['device_used']; ?></div>
									</div>
								<?php } ?>
								<?php if($settings['source_ip'] == 'on' && $device_data['source_ip']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Source IP Address</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo  $device_data['source_ip']; ?></div>
									</div>
								<?php } ?>
								<?php if($settings['proxy_ip'] == 'on' && $device_data['proxy_ip']){ ?>
									<div class="row" style="float:left;clear:left;">
										<div style="margin-left:1em;" class="col-md-12"><h4>Proxy IP Address</h4></div>
									</div>
									<div class="row" style="float:left;clear:left;">
										<div class="col-md-12" style="margin-left:1em;background:white;"><?php echo  $device_data['proxy_ip']; ?></div>
									</div>
								<?php } ?>
						</div>
				<?php } 
				}else{ ?>
					<div class="row" style="float:left;clear:left;">
						<div style="width:81em;" class="col-md-12 error">No Result Found</div>
					</div>
					<div class="row" style="float:left;clear:left;">
						<div style="margin-top:1em;" class="col-md-12"><a class="btn btn-primary" href="<?php echo admin_url('admin.php?page=aspk_display_details');?>">Back</a></div>
					</div>
				<?php
				}
				?>
			</div>
			
			<script>
				jQuery(document).ready(function() {

					jQuery('.collapsible').collapsible({
					});
				 });
			</script>
			<?php
		}
		
		
		function show_pages($page_id){
		
				$args = array(
						'sort_order' => 'ASC',
						'sort_column' => 'post_title',
						'hierarchical' => 1,
						'exclude' => '',
						'include' => '',
						'meta_key' => '',
						'meta_value' => '',
						'authors' => '',
						'child_of' => 0,
						'parent' => -1,
						'exclude_tree' => '',
						'number' => '',
						'offset' => 0,
						'post_type' => 'page',
						'post_status' => 'publish'
					); 
			?>
			
			<select class="form-control" name="page_dropdown"> 
				 <?php 
				  $pages = get_pages($args); 
				  if($pages){
					  foreach ($pages as $page ) {
						?><option <?php if($page_id == $page->ID){echo 'selected';}?> value="<?php echo $page->ID;?>"><?php echo $page->post_title;?></option>
					<?php
					  }
				  }
				 ?>
			</select>
					
			<?php
		
		}
			
		function link_Settings(){
		
			if(isset($_POST['aspk_link_Settings'])){
				$aspk_uname = $_POST['u_name'];
				$aspk_cd = $_POST['current_dt'];
				$aspk_source_ip =$_POST['s_ip'];
				$aspk_proxy_ip= $_POST['p_ip'];
				$aspk_device_used = $_POST['d_used'];
				$aspk_browser_used =$_POST['b_used'];
				$aspk_o_system =$_POST['os'];

				$aspk_pages =$_POST['page_dropdown'];

				
				$link_Settings_arr = array();
				$link_Settings_arr['gene_name'] 	= $aspk_uname;
				$link_Settings_arr['current_date']	= $aspk_cd;
				$link_Settings_arr['source_ip'] 	= $aspk_source_ip;
				$link_Settings_arr['proxy_ip']		= $aspk_proxy_ip;
				$link_Settings_arr['device_used'] 	= $aspk_device_used;
				$link_Settings_arr['browser_used'] 	= $aspk_browser_used;
				$link_Settings_arr['operating_system']= $aspk_o_system;

				$link_Settings_arr['page_dropdown'] 	  = $aspk_pages;
				
				
				
				update_option('aspk_link_Settings',$link_Settings_arr);	
				
			}
				$get_link_Settings = $this->get_settings();
			?>
		
		  <div class="tw-bs container" style = "background-color:white; width:90%; float:left; clear:left;margin-top:1em; border: 1px solid;"><!-- start container -->
			   <div style="clear:left;" class="row">
				 <div class="col-md-12" style="margin-top:1em;margin-left:1em;"><h3>Link Settings</h3></div>
			  </div>
				<?php if(isset($_POST['aspk_link_Settings'])){?>
						<div class="updated " id="time_out" style="margin-top:6em;">Settings have been Saved</div>
				<?php } ?>
			  <form  method="post" action="" >
					<div  class="row" style="clear:left;" >
						<div  class="col-md-5" style="margin-top:1em;margin-left:1em;"><h4>Options</h4></div>
						<div  class="col-md-3" style="margin-top:1em;"><h4>On</h4></div>
						<div  class="col-md-3" style="margin-top:1em;"><h4>Off</h4></div>
						<div  class="col-md-1" style="margin-top:1em;"></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>User Name</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_link_Settings['gene_name'] == 'on') || (! isset($get_link_Settings['gene_name']))){ echo "checked" ; } ?> name = "u_name" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if($get_link_Settings['gene_name'] == 'off'){ echo "checked" ; } ?> name = "u_name" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;"></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Current Date & Time</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_link_Settings['current_date'] == 'on') || (! isset($get_link_Settings['current_date']))){ echo "checked" ; }?> checked name = "current_dt" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if($get_link_Settings['current_date'] == 'off'){ echo "checked" ; } ?> name = "current_dt" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Source IP Address</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_link_Settings['source_ip'] == 'on') || (! isset($get_link_Settings['source_ip']))){ echo "checked" ; }?> checked name = "s_ip" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if($get_link_Settings['source_ip'] == 'off'){ echo "checked" ; } ?> name = "s_ip" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Proxy IP Address</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_link_Settings['proxy_ip'] == 'on') || (! isset($get_link_Settings['proxy_ip']))){ echo "checked" ; }?> checked name = "p_ip" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if($get_link_Settings['proxy_ip'] == 'off'){ echo "checked" ; } ?> name = "p_ip" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Device Used</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_link_Settings['device_used'] == 'on') || (! isset($get_link_Settings['device_used']))){ echo "checked" ; }?> name = "d_used" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if($get_link_Settings['device_used'] == 'off'){ echo "checked" ; } ?> name = "d_used" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Browser Used</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_link_Settings['browser_used'] == 'on') || (! isset($get_link_Settings['browser_used']))){ echo "checked" ; }?> name = "b_used" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if($get_link_Settings['browser_used'] == 'off'){ echo "checked" ; } ?> name = "b_used" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Operating System</b></div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_link_Settings['operating_system'] == 'on') || (! isset($get_link_Settings['operating_system']))){ echo "checked" ; }?> name = "os" value = "on" />
						</div>
						<div  class="col-md-3" style="margin-top:1em;">
							<input type = "radio" <?php if($get_link_Settings['operating_system'] == 'off'){ echo "checked" ; } ?> name = "os" value = "off" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;" ></div>
					</div>

				<div  class="row" style="clear:left;">
					<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Expiry Page</b></div>
					<div style="margin-top:1em;width: 24.2em;" class="col-md-4">
						<?php $this->show_pages($get_link_Settings['page_dropdown']);?>
					</div>
					<div style="margin-top:1em;" class="col-md-2"></div>
				</div>
					<div  class="row" style="clear:left;">
						<div style="margin-top: 1em; margin-left:1em;" class="col-md-1">
							<input class = "button-primary" type="submit" value="Save Settings" name="aspk_link_Settings">
						</div>
						<div style="margin-top:1em;" class="col-md-11"></div>
					</div>
				</form>
			</div><!-- end container -->
			<script>
					jQuery(document).ready(function () {
						setTimeout(function() { 
						jQuery("#time_out").hide(); }, 5000);
					});	
			</script>
			<?php
		}
	}
  }

new agile_link_generator();